## Postgres

```bash
su - postgres
psql
```

```sql
create database wikijs;
create user wikijs with password 'wikijs';
alter user wikijs superuser;
\q
```
