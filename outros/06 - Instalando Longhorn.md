Um PersistentVolume (PV) é uma parte do armazenamento dentro do cluster que tenha sido provisionada por um administrador.

Uma PersistentVolumeClaim (PVC) é uma requisição para armazenamento por um usuário. É similar a um Pod. Pods utilizam recursos do nó e PVCs utilizam recursos do PV.

Vamos instalar o longhorn - sistema de volumes

Acesse o Rancher e vá no cluster **curso** --> **Default**.
- Clique no botão Launch para ir ao catálogo.
- Procure por Longhorn e clique selecionando-o
- Para esta versão do Rancher v2.5.x, selecione a opção do LongHorn 0.8.1
- Clique em no botão **Launch**

- Acompanhe o deploy em **Curso** --> **Default**, no menu **Infra-estrutura** --> **Wordkloads**.
- Após concluir, clique em **Apps**, em **longhorn-system** clique em  _/index.html_ para visualizar a dashboard do Longhorn.
