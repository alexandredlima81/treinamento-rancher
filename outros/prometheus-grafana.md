```bash
kubectl create ns cattle-monitoring-system
```

```bash
wget https://github.com/rancher/charts/archive/refs/heads/dev-v2.8.zip
unzip dev-v2.8.zip
kubectl apply -f charts-dev-v2.8/charts/rancher-monitoring-crd/102.0.2+up40.1.2/crd-manifest/crd-alertmanagerconfigs.yaml
kubectl apply -f charts-dev-v2.8/charts/rancher-monitoring-crd/102.0.2+up40.1.2/crd-manifest/crd-alertmanagers.yaml
kubectl apply -f charts-dev-v2.8/charts/rancher-monitoring-crd/102.0.2+up40.1.2/crd-manifest/crd-podmonitors.yaml
kubectl apply -f charts-dev-v2.8/charts/rancher-monitoring-crd/102.0.2+up40.1.2/crd-manifest/crd-probes.yaml
kubectl apply -f charts-dev-v2.8/charts/rancher-monitoring-crd/102.0.2+up40.1.2/crd-manifest/crd-prometheuses.yaml
kubectl apply -f charts-dev-v2.8/charts/rancher-monitoring-crd/102.0.2+up40.1.2/crd-manifest/crd-prometheusrules.yaml
kubectl apply -f charts-dev-v2.8/charts/rancher-monitoring-crd/102.0.2+up40.1.2/crd-manifest/crd-servicemonitors.yaml
kubectl apply -f charts-dev-v2.8/charts/rancher-monitoring-crd/102.0.2+up40.1.2/crd-manifest/crd-thanosrulers.yaml
```

```bash
helm install monitoring charts-dev-v2.8/charts/rancher-monitoring/ --set grafana.enabled=true -n cattle-monitoring-system --create-namespace=true

kubectl --namespace cattle-monitoring-system get pods -l "release=monitoring"
```

Para coletar as credenciais
```bash
kubectl get secret -n monitoring prometheus-grafana -o jsonpath="{.data.admin-password}" | base64 --decode
```

Remoção de CRD's
```bash
kubectl delete crd alertmanagerconfigs.monitoring.coreos.com
kubectl delete crd alertmanagers.monitoring.coreos.com
kubectl delete crd podmonitors.monitoring.coreos.com
kubectl delete crd probes.monitoring.coreos.com
kubectl delete crd prometheuses.monitoring.coreos.com
kubectl delete crd prometheusrules.monitoring.coreos.com
kubectl delete crd servicemonitors.monitoring.coreos.com
kubectl delete crd thanosrulers.monitoring.coreos.com
```

Dashboard utilizadas.

Recursos do cluster
- General / Home
Todos os pods deployments e statefulsets em uma namespace na mesma dashboard
- General / Kubernetes / Compute Resources / Namespace (Pods)
Todos os pods em todos os nodes
- General / Kubernetes / Compute Resources / Node (Pods)
Filtrar um pod específico
- General / Kubernetes / Compute Resources / Pod
Filtrar por namespace e recursos como deployments e statefulsets
- General / Kubernetes / Compute Resources / Namespace (Workloads)
