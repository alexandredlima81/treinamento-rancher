### Traefik

O Traefik é a aplicação que iremos usar como ingress. Ele irá ficar escutando pelas entradas de DNS que o cluster deve responder. Ele possui um dashboard de monitoramento e com um resumo de todas as entradas que estão no cluster.

Abaixo, vamos executar dois arquivos para:
- RBAC
  - (Role-based access control) ou (controle de acesso baseado em função)
  - Organiza as permissões do Traefik no Kubernetes para fazer a função de proxy
- DS 
  - Datasource
  - Criará um **daemonset**, que adicionará é um container para cada servidor, um **ServiceAccount** para utilizar as  permissões criadas pelo RBAC e um Service, para um egress no traefik

```bash
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-rbac.yaml
kubectl apply -f https://raw.githubusercontent.com/containous/traefik/v1.7/examples/k8s/traefik-ds.yaml
```
Poderemos verificar se a execução foi bem-sucedida com o comando abaixo, será apresentado um container do traefik para cada node no cluster:
```bash
kubectl --namespace=kube-system get pods
```

Para ver na interface do Rancher, acesse o Cluster **curso** -> **System**. Na namespace **Namespace: kube-system** será apresentado os dois containers.


Criar o arquivo abaixo:
`vim ui.yaml`
```yaml
---
apiVersion: v1
kind: Service
metadata:
  name: traefik-web-ui
  namespace: kube-system
spec:
  selector:
    k8s-app: traefik-ingress-lb
  ports:
  - name: web
    port: 80
    targetPort: 8080
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: traefik-web-ui
  namespace: kube-system
spec:
  rules:
  #- host: traefik.rancher.<dominio>
  - host: traefik.rancher.duzeru.org
    http:
      paths:
      - path: /
        backend:
          serviceName: traefik-web-ui
          servicePort: web
```

executar o ingress do Traefik
```
kubectl apply -f ui.yaml
```

Acessar a URL **http://traefik.rancher.duzeru.org**
