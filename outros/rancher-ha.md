Ambiente
IP            | Hostname|CPU | RAM | Tipo
:--           | :--     |:--| :-- |:--
192.168.1.101 | rancher1 |2  | 4 | Rancher node
192.168.1.102 | rancher2 |2  | 4 | Rancher node
192.168.1.103 | rancher3 |2  | 4 | Opcional Rancher node
192.168.1.104 | bastion |1  | 1 | Nginx e Bastion


**Executar nos nodes Rancher**
```bash
$ curl https://releases.rancher.com/install-docker/20.10.sh | sh
$ sudo systemctl enable docker
$ sudo usermod -aG docker $USER
$ sudo init 6
```

**Executar no node Bastion**
Instalar Kubectl
```bash
$ curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
$ chmod +x ./kubectl
$ sudo mv ./kubectl /usr/local/bin/kubectl
$ kubectl version --client
```

**Executar no node Bastion**
Instalar RKE
```bash
$ curl -LO https://github.com/rancher/rke/releases/download/v1.4.3/rke_linux-amd64
$ mv rke_linux-amd64 rke
$ chmod +x rke
$ sudo mv ./rke /usr/local/bin/rke
$ rke --version
```

**Executar no node Bastion**
**Criar uma chave ssh do tipo ed25519 com user ubuntu**
```bash
ssh-keygen -t ed25519 -f ~/.ssh/id_rsa
ssh-copy-id ubuntu@192.168.1.101
ssh-copy-id ubuntu@192.168.1.102
ssh-copy-id ubuntu@192.168.1.103
```

**COPIAR para Servidor**
`vim rancher-cluster.yml`
```yaml
nodes:
  - address: 192.168.1.101
    user: ubuntu
    role: [controlplane, worker, etcd]
  - address: 192.168.1.102
    user: ubuntu
    role: [controlplane, worker, etcd]
  - address: 192.168.1.103
    user: ubuntu
    role: [controlplane, worker, etcd]

services:
  etcd:
    snapshot: true
    creation: 6h
    retention: 24h

# Required for external TLS termination with
# ingress-nginx v0.22+
ingress:
  provider: nginx
  options:
    use-forwarded-headers: "true"
```

#### Incluir o DNS do rancher nos nodes
`vim /etc/hosts`
```
192.168.1.101 node1.duzeru.com
192.168.1.102 node2.duzeru.com
192.168.1.103 node3.duzeru.com
192.168.1.104 rancher.duzeru.com

```

**Executar no node Bastion, Rodar RKE**
```bash
rke up --config ./rancher-cluster.yml
```

**Após o cluster subir, vamos gerenciar o Kubernetes**
```bash
export KUBECONFIG=$(pwd)/kube_config_rancher-cluster.yml
kubectl get nodes
kubectl get pods --all-namespaces
```

# Instalar no node Bastion o HELM
```bash
curl -LO https://get.helm.sh/helm-v3.11.2-linux-amd64.tar.gz
tar -zxvf helm-v3.11.2-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
rm -rf helm-v3.11.2-linux-amd64.tar.gz linux-amd64
helm version
```

# No node Bastion seguir as etapas para o Certificate Manager
# Caso não tiver um certificado SSL deve utilizar as etapas abaixo
```bash
kubectl apply --validate=false -f https://github.com/cert-manager/cert-manager/releases/download/v1.11.0/cert-manager.crds.yaml
helm repo add jetstack https://charts.jetstack.io
helm repo update
kubectl create namespace cert-manager
```

```bash
helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v1.11.0
```

```bash
kubectl get pods --namespace cert-manager
```

# Instalar o Rancher - Preparar
```bash
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
kubectl create namespace cattle-system
```

# Instalar Rancher
```bash
helm install rancher rancher-stable/rancher --version 2.7.1 \
  --namespace cattle-system \
  --set hostname=rancher.duzeru.com
```

# Verificar deployment
```bash
kubectl -n cattle-system rollout status deploy/rancher
kubectl -n cattle-system get deploy rancher
```

# Caso algo de errado acontecer no deploy do Rancher
**Exemplo error: deployment "rancher" exceeded its progress deadline**
```bash
helm ls --all-namespaces
kubectl delete namespace cattle-system
kubectl delete -n cattle-system MutatingWebhookConfiguration rancher.cattle.io
kubectl create namespace cattle-system
```

```bash
helm upgrade --install \
--namespace cattle-system \
--set hostname=rancher.duzeru.com \
rancher rancher-stable/rancher --version 2.7.1
```

# Caso acontecer algo na senha do Rancher, podemos resetar
```bash
kubectl --kubeconfig $KUBECONFIG -n cattle-system exec $(kubectl --kubeconfig $KUBECONFIG -n cattle-system get pods -l app=rancher | grep '1/1' | head -1 | awk '{ print $1 }') -- reset-password
```

# RODAR O NGINX
`vim nginx.conf`
```conf
worker_processes 4;
worker_rlimit_nofile 40000;

events {
    worker_connections 8192;
}

stream {
    upstream rancher_servers_http {
        least_conn;
        server 192.168.1.101:80 max_fails=3 fail_timeout=5s;
        server 192.168.1.102:80 max_fails=3 fail_timeout=5s;
        server 192.168.1.103:80 max_fails=3 fail_timeout=5s;
    }
    server {
        listen 80;
        proxy_pass rancher_servers_http;
    }

    upstream rancher_servers_https {
        least_conn;
        server 192.168.1.101:443 max_fails=3 fail_timeout=5s;
        server 192.168.1.102:443 max_fails=3 fail_timeout=5s;
        server 192.168.1.103:443 max_fails=3 fail_timeout=5s;
    }
    server {
        listen     443;
        proxy_pass rancher_servers_https;
    }

}
```

```bash

docker run -d --restart=unless-stopped \
  -p 80:80 -p 443:443 \
  -v $(pwd)/nginx.conf:/etc/nginx/nginx.conf \
  nginx:1.14
```

Fonte: 
Install Docker: https://ranchermanager.docs.rancher.com/getting-started/installation-and-upgrade/installation-requirements/install-docker
Reset Password: https://ranchermanager.docs.rancher.com/v2.5/faq/technical-items



kubectl delete pods --field-selector status.phase=Failed --all-namespaces

#lista negra temporariamente neste node
kubectl cordon 192.168.1.103

# Drenar todos os pods deste node para os outros
kubectl drain 192.168.1.103 --force --ignore-daemonsets
