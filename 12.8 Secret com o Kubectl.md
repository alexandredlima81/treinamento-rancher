Crie o primeiro arquivo `vim secret_wikijs.yaml`

Adicione todo o conteúdo dentro do arquivo:
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: secret-wiki
  namespace: wikijs
type: Opaque
stringData:
  DB_USER: wikijs
  DB_PASS: wikijs

```

Caso desejar codificar algo no **terminal**, pode ser utilizado o seguinte comando:
```bash 
echo -n '' | base64
```

Caso desejar decodificar algo em base64 pode executar o seguinte comando:
```bash
echo -n '' | base64 --decode
```

Para utilizar o secret no Deploy, inserimos a seguinte parte do comando:
```yaml
            valueFrom:
              secretKeyRef:
                name: secret-wikijs 
                key: DB_USER 

```

O manifesto completo do vídeo ficou da seguinte forma:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: wikijs
  name: wikijs
  namespace: wikijs
spec:
  replicas: 1
  selector:
    matchLabels:
      app: wikijs
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: wikijs
    spec:
      containers:
      - image: ghcr.io/requarks/wiki:2.5
        name: wiki
        ports:
        - containerPort: 3000
        resources: {}
        env:
        - name: DB_TYPE
          value: "postgres"
        - name: DB_HOST
          value: "192.168.1.104"
        - name: DB_PORT
          value: "5432"
        - name: DB_USER
          valueFrom:
            secretKeyRef:
              name: secret-wiki
              key: DB_USER
        - name: DB_PASS
          valueFrom:
            secretKeyRef:
              name: secret-wiki
              key: DB_PASS
        - name: DB_NAME
          value: "wikijs"
status: {}
```
