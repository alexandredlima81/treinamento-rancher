Não se preocuparemos com a segurança no NFS, será utilizado o usuário root e serão atribuídas permissividades que não é boa prática em cenários reais.

### No servidor **extras** (192.168.1.104)
```bash
sudo su
apt update && \
apt install -y nfs-kernel-server
```

Criar o diretório para persistir dados e alterar permissões.
```bash
mkdir -p /storage/app3 && \
chown -R nobody:nogroup /storage/app3 && \
chmod 777 /storage/app3
```

Adicionar parâmetros no arquivo de configuração para permitir que o(s) Servidor(es) acessem o diretório:
`vim /etc/exports`
```conf
/storage/app3 *(rw,sync,no_subtree_check,no_root_squash,insecure)
```

Aplique as mudanças na configuração e reinicie o serviço do NFS
```bash
exportfs -a
systemctl restart nfs-kernel-server
```

### No Servidor **worker1** (192.168.1.102)
```bash
sudo apt update && sudo apt install -y nfs-common
```