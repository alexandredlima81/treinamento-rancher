### Criando PV e PVC
`vim pv.yaml`
```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-app3
spec:
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteMany
  nfs:
    server: 192.168.1.104
    path: "/storage/app3"
  mountOptions:
    - nfsvers=4.2
```

`vim pvc.yaml`
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-app3
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: ""
  resources:
    requests:
      storage: 5Gi
  volumeName: pv-app3
```

Obtenha o manifesto do Deployment para alterarmos mais tarde:
```bash
kubectl create deployment app3 --image nginx --port=80 --dry-run=client -o yaml > deployment_app3.yaml
```

### Inserindo o PVC no Deployment
edite o manifesto de Deployment inserindo a opção de volume:
`vim deployment_app3.yaml`
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: app3
  name: app3
spec:
  replicas: 1
  selector:
    matchLabels:
      app: app3
  template:
    metadata:
      labels:
        app: app3
    spec:
      containers:
      - image: nginx
        name: nginx
        ports:
        - containerPort: 80
        resources: {}
        volumeMounts:
          - name: nfs
            mountPath: "/app3"
      volumes:
      - name: nfs
        persistentVolumeClaim:
          claimName: pvc-app3
```