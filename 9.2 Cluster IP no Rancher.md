## Cluster IP
Exponha um ou conjunto de pods a outros pods dentro do cluster. Esse tipo de serviço só pode ser acessado de dentro do cluster. Este é o tipo padrão.

Inicie esta tarefa montando o ambiente de Lab com **duas namespaces** com **dois Pods** diferentes.

Em **Cluster -> Projects/Namespaces**, crie as duas namespaces no projeto **treinamento**
- namespace1
- namespace2

Filte as duas namespaces.

Vá em Workloads e crie dois pods.
- Pod **nginx** na **namespace1**
- Pod **ubuntu** na **namespace2**  _*OBS Como Ubuntu não tém um serviço inicializando o Pod/Container, atentar-se de adicionar "sleep 999999" no campo command_

É sugerido para melhor visualização que tenha duas abas no navegador abertas, a primeira em **Workloads/Pods**, a segunda em **Service Discovery/Service**.

**Aba Workloads/Pods**
Precisamos de um Label para identificar este Pod de forma única no Cluster e vamos criar um Label.
- Vá até o pod **nginx**, clique nos três pontinhos e em **Edit Config**.
  - Vá até a aba Pod e adicione um Label chave e valor, app=nginx.
  - clique em salvar.

**Aba Service Discovery/Service**
Na aba de Service Discovery, crie um Service do tipo **Cluster IP**
- Namespace= namespace1
- Name= svc-cluster
- Port Name= http
- Listening Port= 80
- Target Port= 80
- Vá até **Selectors** para adicionar um Label que definimos para a afinidade do Service com o Pod. Insira a chave e valor app=nginx

Após criar, clique para acessar o serviço, perceba que este tipo de Service ganhou um IP na rede do cluster.


**Aba Workloads/Pods**
- No **Pod nginx**, clique em **Execute Shell**
  - Execute o comando "curl IP_do_Service"
  - Observe que a página é reconhecida dentro do Pod.
- No **Pod ubuntu**, clique em **Execute Shell**
  - Execute o comando "curl IP_do_Service"
  - Observe que a página é reconhecida dentro do Pod.

Faça o mesmo teste em seu computador, caso não seja Linux para utilizar o Curl, pode inserir o IP_do_Service no navegador.
Observe que não é apresentado a página inicial do Nginx, isto porquê conforme o objetivo do tipo de Service, só pode ser acessado de dentro do cluster.
